FROM node:21-alpine

WORKDIR /workspace

COPY package.json package.json
COPY package-lock.json package-lock.json
COPY src/metabase.js src/metabase.js
COPY src/export.js src/export.js
COPY src/import.js src/import.js

RUN chmod 777 src/export.js

RUN npm ci

ENTRYPOINT [ "node" , "src/export.js" ]
