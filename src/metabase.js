const axios = require("axios");
const fs = require('fs');
const prompt = require('prompt-sync')({sigint: true});

const dashboardPath = 'dashboard/'
const collectionPath = 'collection/'
const cardPath = 'card/'
const userPath = 'user/'
const databasePath = 'database/'
const tablePath = 'table/'


async function getSession(sourceUrl, name, password) {
    const loginResult = await axios.post(`${sourceUrl}session/`, {
        "username": name,
        "password": password,
    })

    return loginResult.data.id
}

async function getAuthInformationFromFile(env) {
    if (!fs.existsSync(`./auth/${env}.json`)) {
        return null;
    }

    let jsonObj = await fs.readFileSync(`./auth/${env}.json`, "utf8");

    return JSON.parse(jsonObj);
}

function getAuthInformationFromPrompt() {
    const sourceUrl = prompt('URL of your metabase instance: ').replace(/\/+$/g, '') + '/api/'
    const name = prompt('Username: ')
    const password = prompt('Password: ', { echo: '*' })

    return { url: sourceUrl, username: name, password: password }
}

async function login(env) {
    let authInformation = await getAuthInformationFromFile(env)
    if (authInformation === null) {
        authInformation = getAuthInformationFromPrompt()
    }

    try {
        let session = await getSession(authInformation.url, authInformation.username, authInformation.password)

        return {sourceUrl: authInformation.url, metabaseSession: session}
    } catch (e) {
        console.log(e)
        process.exit(1)
    }
}


async function getMetabaseData(url, session) {
    const result = await axios.get(`${session.sourceUrl}${url}`, {
        headers: {
            'X-Metabase-Session': session.metabaseSession
        }
    })

    return result.data
}

async function updateMetabaseData(url, session, id, data) {
    const updated = await axios.put(
        `${session.sourceUrl}${url}${id}`,
        data,
        {
            headers: {
                "X-Metabase-Session": session.metabaseSession
            }
        }
    );

    return updated.data;
}

async function createMetabaseData(url, session, data) {
    const created = await axios.post(
        `${session.sourceUrl}${url}`,
        data,
        {
            headers: {
                "X-Metabase-Session": session.metabaseSession
            }
        }
    );

    return created.data;
}

async function getDashboards(session) {
    return await getMetabaseData(dashboardPath, session)
}

async function updateDashboard(session, dashboardId, data) {
    return await updateMetabaseData(dashboardPath, session, dashboardId, data)
}

async function createDashboard(session, data) {
    return await createMetabaseData(dashboardPath, session, data)
}

async function getCollections(session) {
    return await getMetabaseData(collectionPath, session)
}

async function updateCollection(session, collectionId, data) {
    return await updateMetabaseData(collectionPath, session, collectionId, data)
}

async function createCollection(session, data) {
    return await createMetabaseData(collectionPath, session, data)
}

async function getCards(session) {
    return await getMetabaseData(cardPath, session)
}

async function updateCard(session, cardId, data) {
    return await updateMetabaseData(cardPath, session, cardId, data)
}

async function createCard(session, data) {
    return await createMetabaseData(cardPath, session, data)
}

async function getDashboardCard(session, dashboardId) {
    return await getMetabaseData(`${dashboardPath}${dashboardId}`, session)
}

async function createDashboardCard(session, dashboardId, data) {
    return await createMetabaseData(`${dashboardPath}${dashboardId}/cards`, session, data)
}

async function getUsers(session) {
    const users = await getMetabaseData(`${userPath}`, session)

    return users.data;
}

async function getDatabases(session) {
    const databases = await getMetabaseData(`${databasePath}`, session)

    return databases.data;
}

async function getTables(session) {
    return await getMetabaseData(`${tablePath}`, session)
}

async function getTableMetadata(session, tableId) {
    return await getMetabaseData(`${tablePath}${tableId}/query_metadata`, session)
}

function isTableReferencingAnotherCard(importTableId) {
    return typeof importTableId === 'string' && importTableId.startsWith('card__')
}

function getCardIdFromTableId(importTableId) {
    return Number(importTableId.replace('card__', ''))
}

function getCardDependencies(sourceCard) {
    let cardDependencies = []

    if (isTableReferencingAnotherCard(sourceCard.table_id)) {
        cardDependencies.push(getCardIdFromTableId(sourceCard.table_id))
    }

    if (sourceCard.dataset_query && sourceCard.dataset_query.query) {
        if (isTableReferencingAnotherCard(sourceCard.dataset_query.query["source-table"])) {
            cardDependencies.push(getCardIdFromTableId(sourceCard.dataset_query.query["source-table"]))
        }

        if (sourceCard.dataset_query.query.joins) {
            for (let join of sourceCard.dataset_query.query.joins) {
                if (isTableReferencingAnotherCard(join["source-table"])) {
                    cardDependencies.push(getCardIdFromTableId(join["source-table"]))
                }
            }
        }
    }

    return cardDependencies
}

function getCardsDependencies(sourceCards) {
    let dependencies = new Map()
    for (let importCard of sourceCards) {
        let cardDependencies = getCardDependencies(importCard)

        if (cardDependencies.length > 0) {
            dependencies.set(importCard.id, [...new Set(cardDependencies)])
        }
    }

    return dependencies
}

function flattenDependencies(cardId, cardDependencies, flattenedDependencies = []) {
    if (flattenedDependencies.includes(cardId)) {
        return flattenedDependencies
    }

    if (!cardDependencies.has(cardId)) {
        return [ ...flattenedDependencies, cardId ]
    }

    let dependencies = cardDependencies.get(cardId)
    for (let dependency of dependencies) {
        flattenedDependencies = flattenDependencies(dependency, cardDependencies, flattenedDependencies)
    }

    return [ ...flattenedDependencies, cardId ]
}

module.exports = {
    login,
    getDashboards,   getCollections,   getCards,   getDashboardCard, getUsers, getDatabases, getTables, getTableMetadata,
    updateDashboard, updateCollection, updateCard,
    createDashboard, createCollection, createCard, createDashboardCard,
    isTableReferencingAnotherCard, getCardIdFromTableId, getCardDependencies, getCardsDependencies, flattenDependencies
}
