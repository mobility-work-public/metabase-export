const axios = require("axios");
const fs = require("fs");
const prompt = require("prompt-sync")({ sigint: true });
const metabase = require("./metabase");

async function importMetabase() {

    function listImportFiles(dir) {
        let files = fs.readdirSync(dir)
        files = files.filter((file) => file.endsWith('.json'))
        for (let i in files) {
            console.log(`    [${i}] ${files[i]}`)
        }

        return files
    }

    async function getDestinationMappingTable(metabaseSession) {
        let destinationDashboardMappingTable = new Map()

        const destinationDashboards = await metabase.getDashboards(metabaseSession)
        for (let i of destinationDashboards) {
            destinationDashboardMappingTable.set(i.name, i.id)
        }

        return destinationDashboardMappingTable
    }

    async function getDestinationCardsMappingTable(metabaseSession) {
        let destinationCardsMappingTable = new Map()

        const destinationCards = await metabase.getCards(metabaseSession)
        for (let i of destinationCards) {
            destinationCardsMappingTable.set(i.name, i.id)
        }

        return destinationCardsMappingTable
    }

    function getUpdatedFieldRef(fieldRef, fieldMappingTable) {
        if (!Array.isArray(fieldRef)) {
            return fieldRef
        }

        let newFieldRef = []
        for (let singleFieldRef of fieldRef) {
            if (Number.isInteger(singleFieldRef) && fieldMappingTable.has(singleFieldRef)) {
                singleFieldRef = fieldMappingTable.get(singleFieldRef)
            }
            if (singleFieldRef !== null &&
                typeof singleFieldRef == 'object' &&
                singleFieldRef["source-field"] &&
                Number.isInteger(singleFieldRef["source-field"]) &&
                fieldMappingTable.has(singleFieldRef["source-field"])) {
                singleFieldRef["source-field"] = fieldMappingTable.get(singleFieldRef["source-field"])
            }
            newFieldRef.push(singleFieldRef)
        }

        return newFieldRef
    }

    function getUpdatedExpression(expression, fieldMappingTable) {
        if (expression.length === 3 && Number.isInteger(expression[1]) && fieldMappingTable.has(expression[1])) {
            return getUpdatedFieldRef(expression, fieldMappingTable)
        }

        let newExpression = []
        for(let expressionPart of expression) {
            if (Array.isArray(expressionPart)) {
                expressionPart = getUpdatedExpression(expressionPart, fieldMappingTable)
            }

            newExpression.push(expressionPart)
        }

        return newExpression
    }

    function getDestinationTableId(tableMappingTable, importTableId, cardMappingTable) {
        if (tableMappingTable.has(importTableId)) {
            return tableMappingTable.get(importTableId)
        }

        if (metabase.isTableReferencingAnotherCard(importTableId)) {
            let importCardId = metabase.getCardIdFromTableId(importTableId)
            if (cardMappingTable.has(importCardId)) {
                return 'card__' + cardMappingTable.get(importCardId)
            }
        }

        return importTableId
    }

    function getCardsMap(sourceCards) {
        let map = new Map()
        for (let importCard of sourceCards) {
            map.set(importCard.id, importCard)
        }
        return map;
    }

    function getOrderedCardsToImport(cardsMap, cardDependencies) {

        let flattenDependenciesArray = []
        for (let [cardId, value] of cardDependencies) {
            flattenDependenciesArray = metabase.flattenDependencies(cardId, cardDependencies, flattenDependenciesArray)
        }

        let orderedCards = []
        for (let cardId of flattenDependenciesArray) {
            let importCard = cardsMap.get(cardId)
            orderedCards.push(importCard)
        }

        for(let importCard of cardsMap.values()) {
            if (!flattenDependenciesArray.includes(importCard.id)) {
                orderedCards.push(importCard)
            }
        }

        return orderedCards;
    }

    try {
        // login
        const metabaseSession = await metabase.login('import')
        console.log("Login successful!")

        const dir = './export/'
        const files = listImportFiles(dir)
        const fileIndex = prompt('Enter file index: ')
        const fileName = files[fileIndex]

        console.log(`Importing ${fileName}...`)

        let jsonObj = fs.readFileSync(`./export/${fileName}`, "utf8")
        let importData = JSON.parse(jsonObj)

        // user selection
        const users = await metabase.getUsers(metabaseSession);
        console.log("Import user:")
        for(let user of users) {
            console.log(`    [${user.id}] ${user.first_name} ${user.last_name}`)
        }
        const userId = prompt('Select the ID of the user who will own the import: ')
        const importUser = users.find(user => user.id == userId)

        if (!importUser) {
            console.log("User not found!")
            return 1
        }

        // collection selection
        const collections = await metabase.getCollections(metabaseSession);
        console.log("Import collection:")
        for(let collection of collections) {
            console.log(`    [${collection.id}] ${collection.name}`)
        }
        const collectionId = prompt('Select the ID of the collection where the import will be placed: ')
        const importCollection = collections.find(collection => collection.id == collectionId)

        if (!importCollection) {
            console.log("Collection not found!")
            return 1
        }

        console.log(`Import will be done in collection "[${importCollection.id}] ${importCollection.name}" by user "[${importUser.id}] ${importUser.first_name} ${importUser.last_name}".`)

        // databases
        let databaseMappingTable = new Map()
        let reverseDatabaseMappingTable = new Map()
        let destinationDatabases = await metabase.getDatabases(metabaseSession)
        for (let importDatabase of importData.sourceDatabases) {
            console.log(`Select the database to use for ${importDatabase.name}:`)
            for (let destinationDatabase of destinationDatabases) {
                console.log(`    [${destinationDatabase.id}] ${destinationDatabase.name}`)
            }
            const databaseId = prompt('Select the ID of the database to use: ')
            databaseMappingTable.set(importDatabase.id, parseInt(databaseId))
            reverseDatabaseMappingTable.set(parseInt(databaseId), importDatabase.id)
        }

        // tables
        let tableMappingTable = new Map()
        let destinationTables = await metabase.getTables(metabaseSession)
        for (let destinationTable of destinationTables) {
            let tableDatabaseId = destinationTable.db_id

            if (!reverseDatabaseMappingTable.has(tableDatabaseId)) {
                continue
            }

            let importDatabaseId = reverseDatabaseMappingTable.get(tableDatabaseId)

            for (let importTable of importData.sourceTables) {
                if (importTable.name === destinationTable.name && importDatabaseId == importTable.db_id) {
                    tableMappingTable.set(importTable.id, destinationTable.id)
                }
            }
        }

        // fields
        let fieldMappingTable = new Map()
        let emptyMap = new Map()
        for (let importTable of importData.sourceTables) {
            const destinationTableId = getDestinationTableId(tableMappingTable, importTable.id, emptyMap)
            const destinationTable = await metabase.getTableMetadata(metabaseSession, destinationTableId)
            const destinationFields = destinationTable.fields

            for (let importField of importTable.fields) {
                for (let destinationField of destinationFields) {
                    if (importField.name === destinationField.name) {
                        fieldMappingTable.set(importField.id, destinationField.id)
                    }
                }
            }
        }

        //dashboards
        let dashboardMappingTable = new Map()
        let destinationDashboardMappingTable = await getDestinationMappingTable(metabaseSession)
        for (let importDashboard of importData.sourceDashboards) {
            const destinationDashboardId = destinationDashboardMappingTable.get(importDashboard.name)

            importDashboard.collection_id = importCollection.id
            importDashboard.creator_id = importUser.id



            // If a dashboard with the same name already exists in the destination, we update it
            if (destinationDashboardId) {
                const destinationDashboard = await metabase.updateDashboard(metabaseSession, destinationDashboardId, importDashboard)
                dashboardMappingTable.set(importDashboard.id, destinationDashboard.id)
            } else {
                const destinationDashboard = await metabase.createDashboard(metabaseSession, importDashboard)
                dashboardMappingTable.set(importDashboard.id, destinationDashboard.id)
            }
        }

        // for metabase cards
        let cardMappingTable = new Map()
        let cardDependencies = metabase.getCardsDependencies(importData.sourceCards)
        let cardsMap = getCardsMap(importData.sourceCards)
        let cardsToImport = getOrderedCardsToImport(cardsMap, cardDependencies)

        let destinationCardsMappingTable = await getDestinationCardsMappingTable(metabaseSession)
        for (const importCard of cardsToImport) {
            const destinationCardId = destinationCardsMappingTable.get(importCard.name)

            importCard.collection_id = importCollection.id
            importCard.creator_id = importUser.id
            importCard.table_id = getDestinationTableId(tableMappingTable, importCard.table_id, cardMappingTable)
            importCard.database_id = databaseMappingTable.get(importCard.database_id)

            for (let cardResult of importCard.result_metadata) {
                cardResult.id = fieldMappingTable.get(cardResult.id)
                cardResult.field_ref = getUpdatedFieldRef(cardResult.field_ref, fieldMappingTable)
            }

            if (importCard.dataset_query) {
                importCard.dataset_query.database = databaseMappingTable.get(importCard.dataset_query.database)

                if (importCard.dataset_query.query) {
                    importCard.dataset_query.query["source-table"] = getDestinationTableId(tableMappingTable, importCard.dataset_query.query["source-table"], cardMappingTable)

                    if (importCard.dataset_query.query.fields) {
                        let newFields = []
                        for (let field of importCard.dataset_query.query.fields) {
                            newFields.push(getUpdatedFieldRef(field, fieldMappingTable))
                        }
                        importCard.dataset_query.query.fields = newFields
                    }

                    if (importCard.dataset_query.query.breakout) {
                        let newBreakout = []
                        for (let breakout of importCard.dataset_query.query.breakout) {
                            newBreakout.push(getUpdatedFieldRef(breakout, fieldMappingTable))
                        }
                        importCard.dataset_query.query.breakout = newBreakout
                    }

                    if (importCard.dataset_query.query.aggregation) {
                        let newAggregation = []
                        for (let aggregation of importCard.dataset_query.query.aggregation) {
                            let newSingleAggregation = getUpdatedExpression(aggregation, fieldMappingTable)
                            newAggregation.push(newSingleAggregation)
                        }
                        importCard.dataset_query.query.aggregation = newAggregation
                    }

                    if (importCard.dataset_query.query.expressions) {
                        let newExpressions = {}
                        for (const [key, value] of Object.entries(importCard.dataset_query.query.expressions)) {
                            newExpressions[key] = getUpdatedExpression(value, fieldMappingTable)
                        }
                        importCard.dataset_query.query.expressions = newExpressions
                    }

                    if (importCard.dataset_query.query.filter) {
                        importCard.dataset_query.query.filter = getUpdatedExpression(importCard.dataset_query.query.filter, fieldMappingTable)
                    }

                    if (importCard.dataset_query.query.joins) {
                        for (let join of importCard.dataset_query.query.joins) {
                            join.condition = getUpdatedExpression(join.condition, fieldMappingTable)
                            join["source-table"] = getDestinationTableId(tableMappingTable, join["source-table"], cardMappingTable)

                            if (join.fields && Array.isArray(join.fields)) {
                                join.fields = getUpdatedExpression(join.fields, fieldMappingTable)
                            }
                        }
                    }

                    if (importCard.dataset_query.query['order-by']) {
                        importCard.dataset_query.query['order-by'] = getUpdatedExpression(importCard.dataset_query.query['order-by'], fieldMappingTable)
                    }
                }
            }

            if (importCard.visualization_settings) {
                if (importCard.visualization_settings["table.columns"]) {
                    for (let column of importCard.visualization_settings["table.columns"]) {
                        if (column.fieldRef) {
                            column.fieldRef = getUpdatedFieldRef(column.fieldRef, fieldMappingTable)
                        }

                        if (column.field_ref) {
                            column.field_ref = getUpdatedFieldRef(column.field_ref, fieldMappingTable)
                        }
                    }
                }

                if (importCard.visualization_settings.column_settings) {
                    let newSettings = {}
                    for (const [key, value] of Object.entries(importCard.visualization_settings.column_settings)) {
                        let decodedKey = JSON.parse(key)
                        let newKey = JSON.stringify(getUpdatedExpression(decodedKey, fieldMappingTable))
                        newSettings[newKey] = value
                    }
                    importCard.visualization_settings.column_settings = newSettings
                }
            }

            if (destinationCardId) {
                const destinationCard = await metabase.updateCard(metabaseSession, destinationCardId, importCard)
                cardMappingTable.set(importCard.id, destinationCard.id)
            } else {
                const destinationCard = await metabase.createCard(metabaseSession, importCard)
                cardMappingTable.set(importCard.id, destinationCard.id)
            }
        }

        // for metabase dashboard cards
        for (let importDashboard of importData.sourceDashboardCards) {
            for (let importDashboardCard of importDashboard.ordered_cards) {
                let destinationDashboardId = dashboardMappingTable.get(importDashboard.id)
                let destinationCardId = cardMappingTable.has(importDashboardCard.card_id) ? cardMappingTable.get(importDashboardCard.card_id) : importDashboardCard.card_id

                importDashboardCard.card_id = destinationCardId;
                importDashboardCard.cardId = destinationCardId;

                for (let serie of importDashboardCard.series) {
                    serie.id = cardMappingTable.get(serie.id)
                }

                if (importDashboardCard.parameter_mappings && Array.isArray(importDashboardCard.parameter_mappings)) {
                    for (let parameter of importDashboardCard.parameter_mappings) {
                        parameter.card_id = cardMappingTable.get(parameter.card_id)
                        parameter.target = getUpdatedExpression(parameter.target, fieldMappingTable)
                    }
                }

                await metabase.createDashboardCard(
                    metabaseSession,
                    destinationDashboardId,
                    importDashboardCard
                )
            }
        }

        console.log("Import successful!")

    } catch (error) {
        console.log(error)
        return 1
    }
}

importMetabase()