const fs = require('fs');
const prompt = require('prompt-sync')({sigint: true});
const metabase = require('./metabase')

async function exportMetabase() {

    async function getDashboardCards(sourceDashboards, metabaseSession) {
        const sourceDashboardCards = []

        for (let dashboard of sourceDashboards) {
            const sourceDashboardCard = await metabase.getDashboardCard(metabaseSession, dashboard.id)

            sourceDashboardCards.push(sourceDashboardCard)
        }

        return sourceDashboardCards;
    }

    function getFilteredDashboards(sourceDashboards, dashboardsIdToExport) {
        let dashboardsToExport = []
        for (let dashboard of sourceDashboards) {
            if (dashboardsIdToExport.includes(dashboard.id)) {
                dashboardsToExport.push(dashboard)
            }
        }

        return dashboardsToExport
    }

    function getFilteredCollections(sourceCollections, filteredDashboards, filteredCards) {
        let collectionIdsToExport = []
        for (let dashboard of filteredDashboards) {
            if (dashboard.collection_id !== null) {
                collectionIdsToExport.push(dashboard.collection_id)
            }
        }

        for (let card of filteredCards) {
            if (card.collection_id !== null) {
                collectionIdsToExport.push(card.collection_id)
            }
        }

        let collectionsToExport = []
        for (let collection of sourceCollections) {
            if (collectionIdsToExport.includes(collection.id)) {
                collectionsToExport.push(collection)
            }
        }

        return collectionsToExport;
    }

    function getFilteredDashboardCards(sourceDashboardCards, dashboardsIdToExport) {
        let dashboardCardsToExport = []
        for (let dashboard of sourceDashboardCards) {
            if (dashboardsIdToExport.includes(dashboard.id)) {
                dashboardCardsToExport.push(dashboard)
            }
        }

        return dashboardCardsToExport
    }

    function getFilteredCards(sourceCards, filteredDashboardCards) {
        let cardIdsToExport = []
        for (let dashboardCard of filteredDashboardCards) {
            for (let card of dashboardCard.ordered_cards) {
                cardIdsToExport.push(card.card_id)
                if (card.series) {
                    for (let serie of card.series) {
                        cardIdsToExport.push(serie.id)
                    }
                }
            }

        }

        let completeIdsToExport = []
        let dependencies = metabase.getCardsDependencies(sourceCards)
        for(let cardId of cardIdsToExport) {
            let flattenedDependencies = metabase.flattenDependencies(cardId, dependencies)
            for(let dependency of flattenedDependencies) {
                completeIdsToExport.push(dependency)
            }
        }
        completeIdsToExport = [...new Set(completeIdsToExport)]

        let cardsToExport = []
        for (let card of sourceCards) {
            if (completeIdsToExport.includes(card.id)) {
                cardsToExport.push(card)
            }
        }

        return [... new Set(cardsToExport)]
    }

    async function getFilteredTables(session, sourceTables, filteredDatabases) {
        let databasesIds = []
        for(let database of filteredDatabases) {
            databasesIds.push(database.id)
        }

        let tablesToExport = []
        for (let table of sourceTables) {
            if (databasesIds.includes(table.db_id)) {
                let tableMetadata = await metabase.getTableMetadata(session, table.id)
                tablesToExport.push(tableMetadata)
            }
        }

        return tablesToExport
    }

    function getFilteredDatabases(sourceDatabases, filteredCards) {
        let databaseIdsToExport = []
        for (let card of filteredCards) {
            if (card.database_id !== null) {
                databaseIdsToExport.push(card.database_id)
            }
        }

        let databasesToExport = []
        for (let database of sourceDatabases) {
            if (databaseIdsToExport.includes(database.id)) {
                database.details = null
                databasesToExport.push(database)
            }
        }

        return databasesToExport
    }

    try {

        // login
        const metabaseSession = await metabase.login('export')

        // Export
        const sourceDashboards = await metabase.getDashboards(metabaseSession)
        const sourceCollections = await metabase.getCollections(metabaseSession)
        const sourceCards = await metabase.getCards(metabaseSession)
        const sourceDashboardCards = await getDashboardCards(sourceDashboards, metabaseSession);
        const sourceDatabases = await metabase.getDatabases(metabaseSession)
        const sourceTables = await metabase.getTables(metabaseSession)

        // Filter export
        console.log("Found dashboards:")
        for (let dashboard of sourceDashboards) {
            console.log(`    [${dashboard.id}] ${dashboard.name}`)
        }
        const dashboardsToExport = prompt('Dashboard IDs to export (separated by comas): ')
        const dashboardIdsToExportArray = dashboardsToExport.split(",").map(Number)
        
        let filteredDashboards = getFilteredDashboards(sourceDashboards, dashboardIdsToExportArray);
        let filteredDashboardCards = getFilteredDashboardCards(sourceDashboardCards, dashboardIdsToExportArray);
        let filteredCards = getFilteredCards(sourceCards, filteredDashboardCards);
        let filteredCollections = getFilteredCollections(sourceCollections, filteredDashboards, filteredCards);
        let filteredDatabases = getFilteredDatabases(sourceDatabases, filteredCards);
        let filteredTables = await getFilteredTables(metabaseSession, sourceTables, filteredDatabases);

        let filteredJsonFileObject = {
            sourceDashboards : filteredDashboards,
            sourceCollections : filteredCollections,
            sourceCards : filteredCards,
            sourceDashboardCards : filteredDashboardCards,
            sourceTables : filteredTables,
            sourceDatabases : filteredDatabases
        }

        // save data into a json file and export it
        const path = "./export"
        !fs.existsSync(path) && fs.mkdirSync(path, { recursive: true })
        await fs.writeFileSync(`${path}/export.json`, JSON.stringify(filteredJsonFileObject))

        console.log("Exported successfully!")
    } catch (error) {
        console.log(error)
        return error
    }
}

exportMetabase()
