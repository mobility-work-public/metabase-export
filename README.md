# Metabase export

## Getting started

Clone the repo:
```bash
git clone https://gitlab.com/mobility-work-public/metabase-export.git
```

Go to the cloned directory:
```bash
cd metabase-export
```

If you cloned the script earlier and want the latest version, you can run:
```bash
git pull --rebase
docker compose build
```
It will get the last updates and rebuild the containers.

## Run the export:

You have to be at the root of the cloned project directory.

Run the export:
```bash
docker compose run metabase-export -d
```

### Authentication
The script will ask for the metabase instance URL, your username and password (no information is stored).

Alternatively, you can create a config file to store your credentials in `auth/export.json`, by copying the file
`auth/auth.json.tpl` (see format below) and by replacing the values with your credentials.

```json
{
  "metabaseUrl": "https://your-metabase-instance.tld",
  "username": "your-username",
  "password": "your-password"
}
```

### Dashboard selection
The script will ask you to select the dashboards you want to export.
Just type the number(s) of the dashboard(s) you wish to export (separated by comas, no spaces, if given multiple).

### Retrieve the export file:

The export file will be written at `export/export.js`.

It contains no private data, just the information to recreate questions and dashboards : the cards and the dashboards,
configuration, along with the database and tables information used by the cards to be able to map data when importing.

## Import a file:

You have to be at the root of the cloned project directory.

Run the import:
```bash
docker compose run metabase-import -d
```

### Authentication
The script will ask for the metabase instance URL you want to import the file into, your username and password (no information is stored).

Alternatively, you can create a config file to store your credentials in `auth/import.json`, by copying the file
`auth/auth.json.tpl` (see format below) and by replacing the values with your credentials.

```json
{
  "metabaseUrl": "https://your-metabase-instance.tld",
  "username": "your-username",
  "password": "your-password"
}
```

### File selection
The script will ask you to select the file you want to import, by listing the files in the `export` directory.

Type in the number of the file you want to import (only one).

### Database selection
For each database used in the export file, the script will ask you to select the matching database for the data source.

For each database, type in the number of the database corresponding to your metabase instance (only one per source database).

### User and collection selection
The script will ask you to select the existing user of your metabase instance who will own the imported dashboards and cards,
and the collection you want to import the cards and dashboards into.

For each choice, type in the corresponding number (only one).

## Import
After that, the cards and dashboards will be automatically imported.

If a card or dashboard already exists (exact same name), it will be updated.

## Troubleshooting
If the script fails, you can try to run it again with the `-v` option to get more information about the error.